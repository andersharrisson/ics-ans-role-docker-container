ics-ans-role-docker-container
===================

Ansible role to create and run Docker containers.

This role is a pseudo role that requires these mandatory variables:
- docker_container_name: Name of the created container
- docker_container_image_name: Name of the image used to create the container


Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
#docker_container_name:
#docker_container_image_name:
docker_container_image_tag: latest
docker_container_image: "{{ docker_container_image_name }}:{{ docker_container_image_tag }}"
docker_container_pull: "True"
docker_container_purge_networks: "True"
docker_comtainer_command: ""
docker_container_volumes: []
docker_container_env: {}
docker_container_network: "{{ docker_container_name }}-network"
docker_container_networks:
  - name: "{{ docker_container_network }}"
docker_container_restart_policy: on-failure
docker_container_exposed_ports: []
docker_container_labels: {}
docker_container_interactive: "False"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-docker-container
      docker_container_name: docker
      docker_container_image_name: docker
      docker_container_interactive: "True"
      docker_comtainer_command: "sh"
      docker_container_volumes:
        - /var/run/docker.sock:/var/run/docker.sock
```

License
-------

BSD 2-clause
